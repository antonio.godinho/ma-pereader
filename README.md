# ma-pereader

**About**
-----------
<p>A simple terminal PE file parser written in Python 3 and making use of pefile, magic and capstone. Can be seen as a boiler-plate of a PE file parser!</p>

* PE File information
* hashes 
* imports
* exports
* memory info 
* certificates
* listing of the dissassembled code
<br>
<p>This was an exercise to get my head around the PE File format and to dust-off Python knowledge.</p>
<p>If you're looking for a  program to properly handle PE files, I recommend PE-Bear: <a href="https://github.com/hasherezade/pe-bear-releases/releases">https://github.com/hasherezade/pe-bear-releases/releases</a><p>

**Screenshot:**
-----------
![](screenshot.png)

**References:**
-----------
**<p>Diagram</p>**
* <p><a href="https://en.wikipedia.org/wiki/Portable_Executable#/media/File:Portable_Executable_32_bit_Structure_in_SVG_fixed.svg">https://en.wikipedia.org/wiki/Portable_Executable#/media/File:Portable_Executable_32_bit_Structure_in_SVG_fixed.svg</a></p>

**<p>Code:</p>**
* <p><a href="https://bufferoverflows.net/exploring-pe-files-with-python/">https://bufferoverflows.net/exploring-pe-files-with-python/</a></p>
* <p><a href="https://www.capstone-engine.org/lang_python.html">https://www.capstone-engine.org/lang_python.html</a></p>
* <p><a href="https://www.capstone-engine.org/lang_python.html">https://www.capstone-engine.org/lang_python.html</a></p>
* <p><a href="https://stackoverflow.com/questions/53862702/extract-software-signing-cert-using-python-from-a-pe-file">https://stackoverflow.com/questions/53862702/extract-software-signing-cert-using-python-from-a-pe-file</a></p>

**<p>Videos:</p>**
<p>* Malware Theory - Basic Structure of PE Files: <a href="https://www.youtube.com/watch?v=l6GjU8fm8sM">https://www.youtube.com/watch?v=l6GjU8fm8sM</a></p>
<p>* Malware Theory - Memory Mapping of PE Files: <a href="https://www.youtube.com/watch?v=cc1tX1t_bLg">https://www.youtube.com/watch?v=cc1tX1t_bLg</a></p>
<p>* Malware Theory - Portable Executable Resources: <a href="https://www.youtube.com/watch?v=3PcgwKffytI">https://www.youtube.com/watch?v=3PcgwKffytI</a></p>
<p>* Malware Theory - PE Malformations and Anomalies: <a href="https://www.youtube.com/watch?v=-0DEEbQq8jU">https://www.youtube.com/watch?v=-0DEEbQq8jU</a></p>
