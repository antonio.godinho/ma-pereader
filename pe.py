#!/usr/bin/env python

import os
import sys
import magic
import pefile
import hashlib
import capstone
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from asn1crypto import cms

if len(sys.argv) != 2:
    raise ValueError("*** ERROR:" + sys.argv[0] + " - missing PE file...")

"""
References:
-----------
https://bufferoverflows.net/exploring-pe-files-with-python/
https://www.capstone-engine.org/lang_python.html
https://stackoverflow.com/questions/53862702/extract-software-signing-cert-using-python-from-a-pe-file
"""

__PE_FILE__ = sys.argv[1]

pe_file = pefile.PE(__PE_FILE__)


def get_hash_value(hash_type):
    if hash_type == "sha256":
        hash_value = hashlib.sha256()
    else:
        hash_value = hashlib.md5()

    with open(__PE_FILE__, "rb") as data:
        for byte_block in iter(lambda: data.read(4096), b""):
            hash_value.update(byte_block)
        return hash_value.hexdigest()


def convert_bytes(num):
    for x in ["bytes", "KB", "MB", "GB", "TB"]:
        if num < 1024.0:
            return "%3.1f %s" % (num, x)
        num /= 1024.0


def get_file_size(file_path):
    if os.path.isfile(file_path):
        file_info = os.stat(file_path)
        return convert_bytes(file_info.st_size)


"""
TODO
Use integer instead of String
"""


def get_binary_type(machine_type):
    binary_type = ""
    if hex(pe_file.FILE_HEADER.Machine) == "0x14c":
        binary_type = "32-bit"
    else:
        binary_type = "64-bit"
    return binary_type


def disassemble(binary_type):
    eop = pe_file.OPTIONAL_HEADER.AddressOfEntryPoint
    code_section = pe_file.get_section_by_rva(eop)
    code_dump = code_section.get_data()
    code_address = pe_file.OPTIONAL_HEADER.ImageBase + code_section.VirtualAddress
    md = capstone.Cs(
        capstone.CS_ARCH_X86,
        capstone.CS_MODE_32 if binary_type == "32-bit" else capstone.CS_MODE_64,
    )

    for i in md.disasm(code_dump, code_address):
        print("0x%x:\t%s\t%s" % (i.address, i.mnemonic, i.op_str))


print(
    "\nID                       : "
    + magic.from_file(__PE_FILE__)
    + " ("
    + hex(pe_file.DOS_HEADER.e_magic)[2:]
    + ")"
)
print("SHA256:                  : " + get_hash_value("sha256"))
print("MD5:                     : " + get_hash_value("md5"))
print(
    "Disk file size           : "
    + str(os.stat(__PE_FILE__).st_size)
    + " bytes ("
    + str(get_file_size(__PE_FILE__))
    + ")"
)
print("Binary type              : " + get_binary_type(pe_file.FILE_HEADER.Machine))
print(
    "Time-date stamp          : "
    + pe_file.FILE_HEADER.dump_dict()["TimeDateStamp"]["Value"].split("[")[1][:-1]
    + "\n"
)

if hasattr(pe_file, "OPTIONAL_HEADER"):
    print("Size of headers          : " + hex(pe_file.OPTIONAL_HEADER.SizeOfHeaders))
    print("Image Base Address       : " + hex(pe_file.OPTIONAL_HEADER.ImageBase))

print(
    "Number of sections       : " + hex(pe_file.FILE_HEADER.NumberOfSections)[2:] + "\n"
)
for section in pe_file.sections:
    print(
        section.Name.decode().rstrip("\x00")
        + "\t(Entropy: %5.2f)" % section.get_entropy()
        + "\n - Disk:"
        + "\n   Pointer To Raw Data    : "
        + hex(section.PointerToRawData)
        + "\n   Size Of Raw Data       : "
        + hex(section.SizeOfRawData)
        + "\n - Memory:"
        + "\n   Virtual Address        : "
        + hex(section.VirtualAddress)
        + "\n   Virtual Size           : "
        + hex(section.Misc_VirtualSize)
    )

if hasattr(pe_file, "OPTIONAL_HEADER"):
    print("\nData directory:")
    for entry in pe_file.OPTIONAL_HEADER.DATA_DIRECTORY:
        print(
            entry.name
            + "\n   Size: "
            + str(entry.Size)
            + "\n   Virtual address: "
            + hex(entry.VirtualAddress)
        )

print("\nImports:")
for entry in pe_file.DIRECTORY_ENTRY_IMPORT:
    print(entry.dll)
    for imp in entry.imports:
        print("\t", hex(imp.address), imp.name)
print("\nExports:")
if (
    pe_file.OPTIONAL_HEADER.DATA_DIRECTORY[
        pefile.DIRECTORY_ENTRY["IMAGE_DIRECTORY_ENTRY_EXPORT"]
    ].VirtualAddress
    != 0
):
    for exp in pe_file.DIRECTORY_ENTRY_EXPORT.symbols:
        print(hex(pe_file.OPTIONAL_HEADER.ImageBase + exp.address), exp.name)
else:
    print("No exports found.")


"""
TODO
Get more certificate information like Issues details here: http://pedump.me/374fb48a959a96ce92ae0e4346763293/#signature
"""

print("\nCertificates:")
if hasattr(pe_file, "IMAGE_DIRECTORY_ENTRY_SECURITY"):
    sigoff = pe_file.OPTIONAL_HEADER.DATA_DIRECTORY[
        pefile.DIRECTORY_ENTRY["IMAGE_DIRECTORY_ENTRY_SECURITY"]
    ].VirtualAddress
    siglen = pe_file.OPTIONAL_HEADER.DATA_DIRECTORY[
        pefile.DIRECTORY_ENTRY["IMAGE_DIRECTORY_ENTRY_SECURITY"]
    ].Size

    with open(__PE_FILE__, "rb") as fh:
        fh.seek(sigoff)
        thesig = fh.read(siglen)

    signature = cms.ContentInfo.load(thesig[8:])

    for cert in signature["content"]["certificates"]:
        parsed_cert = x509.load_der_x509_certificate(cert.dump(), default_backend())
        print(parsed_cert)
    print("")
else:
    print("No certificates found.")

print("\nDisassembled code:")
disassemble(get_binary_type(pe_file.FILE_HEADER.Machine))
print("")
